<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$roles = [
            [
                'name' => 'admin',
                'description' => 'Contains All Administrator Privileges',
            ],
            [
                'name' => 'user',
                'description' => 'Has All User Accessability'
            ]
        ];

        
        foreach ($roles as $key) {
            Role::create($key);
        }

        Role::findByName('admin')->givePermissionTo([
            'create user' , 
            'edit user' , 
            'delete user'
        ]);
        
        Role::findByName('user')->givePermissionTo([
            'display payment' ,
            'create payment' ,
            'edit payment' ,
            'delete payment' ,
            'create account' ,
            'edit account' ,
            'delete account'
        ]);
    }
}
