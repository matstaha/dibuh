<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Maklad\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	app()['cache']->forget('maklad.permission.cache');
    	
    	$permissions = [
	    	'display role', 
	    	'Create Role', 
	    	'Edit Role' ,
	    	'Delete Role', 
	    	'Display payment' ,
	    	'Create payment' ,
	    	'Edit payment' ,
	    	'Delete payment' ,  
	    	'Customer Access' ,
	    	'Salesman Access' ,
	    	'AdvertiserAccess' ,
	    	'Display Setting' ,
	    	'Create Setting' ,
	    	'Edit Setting' ,
	    	'Delete Setting', 
	    	'Display language' ,
	    	'Create language' ,
	    	'Edit language' ,
	    	'Delete language' ,
	    	'Display user', 
	    	'Create user' ,
	    	'Edit user' ,
	    	'Delete user' ,
	    	'Display User Menu', 
	    	'Display Role Menu' ,
	    	'Display Setting Menu' ,
	    	'Display language Menu' ,
	    	'Display payment Menu' ,
	    	'Display account menu' ,
	    	'Display account', 
	    	'Create account' ,
	    	'Edit account' ,
	    	'Delete account', 
	    	'Display user-settings', 
	    	'Create user-settings' ,
	    	'Edit user-settings' ,
	    	'Delete user-settings', 
    	];

        foreach ($permissions as $key) {
        	Permission::create(['name' => strtolower($key)]);
        }
    }
}
