<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::collection('permissions')->truncate();
        DB::collection('roles')->truncate();
        // DB::collection('users')->truncate();
        
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        
        
    }
}
