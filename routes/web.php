<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();


Route::post('search' , 'SearchController@search');

Route::get('home', 'HomeController@index');

Route::get('admin', 'AdminController@index');
Route::get('admin/login', 'AdminController@create');
Route::post('admin/login', 'AdminController@store');

Route::prefix('admin')->group(function(){
	Route::resource('users' , 'UserController');
	Route::resource('roles' , 'RoleController');
	Route::resource('permissions' , 'PermissionController');
	Route::resource('tax_types' , 'TaxTypeController');
	Route::resource('taxs' , 'TaxController');
});




