<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Tax extends Eloquent
{
    use SoftDeletes;

    protected $fillable = ['title' , 'rate' , 'tax_type_id'];
    protected $dates = ['deleted_at'];
    protected $with = ['taxType'];
    // protected $appends = [];

    public function taxType()
    {
    	return $this->belongsTo('App\TaxType');
    }
}
