<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

class Super
{
    public static function shout(string $string)
    {
        return strtoupper($string);
    }
}