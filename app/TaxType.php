<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class TaxType extends Eloquent
{
    use SoftDeletes;

    protected $fillable = ['title'];
    protected $dates = ['deleted_at'];
    protected $with = [];
    protected $appends = [];

    public function Tax()
    {
    	# code...
    }


}
