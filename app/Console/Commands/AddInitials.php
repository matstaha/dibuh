<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class AddInitials extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:initials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add initials to every user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users= User::all();
        $users->each(function($user){
            $initials = $this->initials($user->name);
            $user->initials = $initials;
            $user->save();
        });
    }
    public function initials($str)
    {
        $ret = '';
        foreach (explode(' ', $str) as $word)
            $ret .= strtoupper($word[0]);
        return $ret;
    }
}
