<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Auth;

class Board extends Eloquent
{
	use SoftDeletes;

    protected $fillable = ['title' , 'privated' , 'bookmarked'];
    protected $dates = ['deleted_at'];
    protected $with = ['owner' , 'members' , 'collections'];
    protected $appends = ['can'];

    public function getCanAttribute()
    {
        $actions = ['update' , 'delete' , 'view'];
        $permissions = [];
        foreach ($actions as $key) {
            $permissions[$key] = Auth::user()->can($key ,  $this);
        }
        return $permissions;
    }
    public function owner()
    {
        return $this->belongsTo('App\User' , 'user_id');
    }
    public function team()
    {
        return $this->belongsTo('App\Team');
    }
    public function members()
    {
    	return $this->belongsToMany('App\User' , null , 'board_id' , 'member_id');
    }
    public function collections()
    {
        return $this->hasMany('App\Collection');
    }

    public function isMember($user_id)
    {
        if($this->member_id){
            return in_array($user_id , $this->member_id);
        }
        return false;
    }
    public function isOwner($user_id)
    {
        return $this->user_id === $user_id;
    }
}
