<?php

namespace App\Http\Controllers;

use App\Board;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;

class BoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('visibility')->only('show');
        
        $this->middleware('can:update,board')->only(['toggleBookmark' , 'togglePrivate' , 'toggleMember']);
    }
    public function sync()
    {
        $auth = Auth::user();

        $teams = $auth->teams()->get();

        $ownTeams = $auth->ownTeams()->get(); 

        $boards = $auth->boards()->get();

        $ownBoards = $auth->ownBoards()->get(); 

        return ['boards' => $boards->union($ownBoards) , 'teams'=> $teams->union($ownTeams)];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        return view('home' , $this->sync());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $board = Auth::user()->ownBoards()->create([
            'title' => $request->title,
            'privated' => $request->privated,
            'bookmarked' => false,
        ]);

        if($request->team_id){
            $board->team_id = $request->team_id;
            $board->save();
        }

        return response()->Json(Board::find($board->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function show(Board $board)
    {
        return view('dashboard' , ['board' => $board]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function edit(Board $board)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Board $board)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy(Board $board)
    {
        //
    }
    public function toggleBookmark(Board $board , Request $request)
    {
        $board->update([
            'bookmarked' => $request->bookmarked
        ]);

        return response()->Json($board);
    }
    public function togglePrivate(Board $board , Request $request)
    {        
        $board->update([
            'privated' => $request->privated
        ]);

        return response()->Json($board);
    }
    public function toggleMember(Board $board , Request $request)
    {   
        $related_members = $board->members();

        $member = $related_members->find($request->member_id);

        if ($member) {
            $related_members->detach($request->member_id);
        }
        else{
            $related_members->attach($request->member_id);
        }     
        
        return response()->Json(Board::find($board->id));
    }
}
