<?php

namespace App\Http\Controllers;

use App\TaxType;
use Illuminate\Http\Request;

class TaxTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->Json(TaxType::get()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        TaxType::create([
            'title' => $request->title
        ]);

        return response()->Json(TaxType::get()); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaxType  $taxType
     * @return \Illuminate\Http\Response
     */
    public function show(TaxType $taxType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaxType  $taxType
     * @return \Illuminate\Http\Response
     */
    public function edit(TaxType $taxType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaxType  $taxType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaxType $taxType)
    {
        $taxType->update([
            'title' => $request->title
        ]);

        return response()->Json(TaxType::get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaxType  $taxType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaxType $taxType)
    {
        $taxType->delete();

        return response()->Json(TaxType::get());
    }
}
