<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
    	$model = 'App\\' . ucfirst($request->get('model'));

    	$elq = new $model;

    	$attrs = $request->get('attributes');

    	$query = $request->get('query');

    	$clause = $elq->where($attrs[0] , 'like', '%'. $query .'%');

        if(count($attrs) > 1)
        {
        	for($i = 1; $i < count($attrs); $i++)
        	{
        		$clause->orWhere($attrs[$i], 'like', '%'. $query .'%');
        	}
        }
  
		return response()->json(['results' => $clause->get()] , 200);
        
    }
}
