<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\MessageBag;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only(['index']);

    }
    public function index()
    {
        $data = [
            'roles' => Role::with('permissions')->get(),
            'permissions' => Permission::get()
        ];

        return view('dashboard' , ['data' => json_encode($data)]);
    }
    public function create()
    {        
        return view('auth.admin.login');
    }
    public function store(Request $request , MessageBag $message_bag)
    {
        $user =  User::where('email' , $request->email)->first();

        if(User::get()->count() === 1){
            $user->assignRole('admin');
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) && Auth::user()->hasRole('admin')) {
            return redirect('admin');
        }
        else{
            $errors  = $message_bag->add('email' , 'Invalid Admin Credenitals');
            return redirect()->back()->withInput()->withErrors($errors);
        }
    }
}
