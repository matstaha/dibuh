<?php

namespace App\Http\Middleware;

use Closure;

class CheckVisibility
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $board = $request->route()->parameter('board');

        $user = $request->user();

        if($board->privated){
            if($user->cant('view', $board)){
                abort(403);
            }
        }

        return $next($request);
    }
}
