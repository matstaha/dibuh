<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Maklad\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable , HasRoles;

    protected $fillable = ['name', 'email', 'password','initials'];
    protected $hidden = ['password', 'remember_token',];
    protected $with = ['roles.permissions'];

}
