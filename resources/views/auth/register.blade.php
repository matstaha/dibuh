@extends('layouts.app')

@section('content')
<b-container>
    <b-row align-h="center">
        <b-col cols="8" >
            <b-card header="Register" border-variant="light">
                <b-form method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                    <b-form-group label="Name">
                        <b-form-input type="text" name="name" value="{{ old('name') }}" state="{{$errors->has('name') ? 'invalid':null}}"></b-form-input>
                        <b-form-feedback>{{ $errors->first('name') }}</b-form-feedback>
                    </b-form-group>
                    <b-form-group label="E-Mail Address">
                        <b-form-input type="email" name="email" value="{{ old('email') }}" state="{{$errors->has('email') ? 'invalid':null}}"></b-form-input>
                        <b-form-feedback>{{ $errors->first('email') }}</b-form-feedback>
                    </b-form-group>
                    <b-form-group label="Password">
                        <b-form-input type="password" name="password" state="{{$errors->has('password') ? 'invalid' : null}}"></b-form-input>
                        <b-form-feedback>{{ $errors->first('password') }}</b-form-feedback>
                    </b-form-group>
                    <b-form-group label="Confirm Password">
                        <b-form-input type="password" name="password_confirmation"></b-form-input>
                    </b-form-group>
                    <b-button type="submit" variant="primary">Register</b-button>
                </b-form>
            </b-card>

        </b-col>
    </b-row>
</b-container>
@endsection
