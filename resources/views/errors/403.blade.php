@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="pr-flex pr-justify-center">
			<h4>You don't have Permission to see this Board</h4>
		</div>
	</div>
@endsection