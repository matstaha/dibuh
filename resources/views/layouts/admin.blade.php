<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{url('/css/app.css')}}" rel="stylesheet">

    <!-- Scripts -->

</head>
<body>
    <div id="app" v-cloak>
        <b-navbar toggleable="md" type="dark" variant="dark">

            <b-nav-toggle target="nav_collapse"></b-nav-toggle>

            <b-navbar-brand href="{{url('/')}}">{{ config('app.name', 'Laravel') }} | Admin</b-navbar-brand>

            <b-collapse is-nav id="nav_collapse">
                <!-- Right aligned nav items -->
                <b-nav is-nav-bar class="ml-auto">
                @if (!Auth::guest())
                  <b-nav-item-dropdown text="{{ Auth::user()->name }}" right>
                    <b-dropdown-item href="{{ url('/home') }}">Home</b-dropdown-item>
                    <b-dropdown-item href="{{ url('/logout') }}" @click.prevent="$refs['logout-form'].submit()">Logout</b-dropdown-item>
                  </b-nav-item-dropdown>
                  <form ref="logout-form" action="{{ url('/logout') }}" method="POST">{{ csrf_field() }}</form>
                @endif
                </b-nav>
            </b-collapse>
        </b-navbar>
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{url('/js/manifest.js')}}"></script>
    <script src="{{url('/js/vendor.js')}}"></script>
    <script src="{{url('/js/app.js')}}"></script>
</body>
</html>
