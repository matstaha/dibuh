<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{url('/css/app.css')}}" rel="stylesheet">

    <!-- Scripts -->
</head>
<body style="direction: rtl">
    <div id="app" v-cloak>
        <b-navbar toggleable="md" type="dark" variant="info">

            <b-nav-toggle target="nav_collapse"></b-nav-toggle>

            <b-navbar-brand href="{{url('/')}}">{{ config('app.name', 'Laravel') }}</b-navbar-brand>

            <b-collapse is-nav id="nav_collapse">
                <b-nav is-nav-bar>
                    @if(!Auth::guest() && Request::is('home'))
                     <b-nav-item href="{{url('/home')}}" :active="{{Request::is('home') ? 'true' : 'false'}}">الرئيسيه</b-nav-item>
                     <b-nav-item href="{{url('/taxs')}}" :active="{{Request::is('taxs') ? 'true' : 'false'}}">الضرائب</b-nav-item>
                    @endif
                </b-nav>

                <!-- Right aligned nav items -->
                <b-nav is-nav-bar class="mr-auto">
                @if (Auth::guest())
                    <b-nav-item href="{{ url('/login') }}">تسجيل الدخول</b-nav-item>
                    <b-nav-item href="{{ url('/register') }}">الأشتراك</b-nav-item>
                @else
                  <b-nav-item-dropdown text="{{ Auth::user()->name }}" left>
                    <b-dropdown-item href="{{ url('/admin') }}" v-if="{{var_export(Auth::user()->hasRole('admin'))}}">لوحة تحكم المديرين</b-dropdown-item>
                    <b-dropdown-item href="{{ url('/logout') }}" @click.prevent="$refs['logout-form'].submit()">تسجيل الخروج</b-dropdown-item>
                  </b-nav-item-dropdown>
                  <form ref="logout-form" action="{{ url('/logout') }}" method="POST">{{ csrf_field() }}</form>
                @endif
                </b-nav>
            </b-collapse>
        </b-navbar>
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{url('/js/manifest.js')}}"></script>
    <script src="{{url('/js/vendor.js')}}"></script>
    <script src="{{url('/js/app.js')}}"></script>
</body>
</html>
