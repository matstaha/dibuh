@extends('layouts.admin')

@section('content')
	<side-menu @selected="current_component = $event"></side-menu>
	<b-container>
		<users v-if="current_component === 'users'" :auth="{{Auth::user()}}"></users>
		<roles v-if="current_component === 'roles'" :data="{{$data}}"></roles>
		<account-settings v-if="current_component === 'account-settings'"></account-settings>
	</b-container>  
@endsection