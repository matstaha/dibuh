
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./plugins');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 
window.bus = new Vue()

Vue.component('SideMenu', r =>  {require(['./components/SideMenu.vue'], r)} )
Vue.component('Users', r =>  {require(['./components/Users.vue'], r)} )
Vue.component('Roles', r =>  {require(['./components/Roles.vue'], r)} )
Vue.component('UserSettings', r =>  {require(['./components/UserSettings.vue'], r)} )
Vue.component('Accounts', r =>  {require(['./components/Accounts.vue'], r)} )
Vue.component('Taxs', r =>  {require(['./components/Taxs.vue'], r)} )
Vue.component('TaxTypes', r =>  {require(['./components/TaxTypes.vue'], r)} )
Vue.component('AccountSettings', r =>  {require(['./components/AccountSettings.vue'], r)} )

const app = new Vue({
    el: '#app',
    data:{
        state_:false,
        current_component: 'account-settings'
    },
    created(){
        axios.interceptors.request.use(c =>  {
            app.state_ = true;
            return c;
        });
        axios.interceptors.response.use(r => {
            app.state_ = false;
            return r;
        }, e =>  {
            app.state_ = false;
            return Promise.reject(e.response);
        }); 
    },

});

