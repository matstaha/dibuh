import SimpleVueValidation  from 'simple-vue-validator';
import BootstrapVue from 'bootstrap-vue'
import IView from 'iview';


Vue.use(BootstrapVue);
Vue.use(SimpleVueValidation);
Vue.use(IView);

window.Validator = SimpleVueValidation.Validator;
Vue.config.productionTip = false;


